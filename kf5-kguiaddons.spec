Summary: KDE Frameworks 5 Tier 1 addon with various classes on top of QtGui
Name:    kf5-kguiaddons
Version: 5.110.0
%global  majmin %majmin_ver_kf5
%global  stable %stable_kf5
Release: 1%{?dist}
License: GPLv2+ and LGPLv2+
URL:     https://invent.kde.org/frameworks/kguiaddons
Source0: http://download.kde.org/%{stable}/frameworks/%{majmin}/kguiaddons-%{version}.tar.xz
BuildRequires:  extra-cmake-modules >= %{majmin}, kf5-rpm-macros >= %{majmin}
BuildRequires:  libX11-devel, libxcb-devel
BuildRequires:  plasma-wayland-protocols-devel
BuildRequires:  qt5-qtbase-devel, qt5-qtbase-private-devel, qt5-qtwayland-devel pkgconfig(wayland-client), qt5-qtx11extras-devel

Requires:       kf5-filesystem >= %{majmin}

%description
KDBusAddons provides convenience classes on top of QtGui.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt5-qtbase-devel

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n kguiaddons-%{version}


%build
%cmake_kf5

%cmake_build


%install
%cmake_install

%files
%license LICENSES/*.txt
%doc README.md
%{_kf5_bindir}/kde-geo-uri-handler
%{_kf5_libdir}/libKF5GuiAddons.so.*
%{_kf5_datadir}/applications/*-handler.desktop
%{_kf5_datadir}/qlogging-categories5/*categories

%files devel
%{_kf5_libdir}/libKF5GuiAddons.so
%{_kf5_libdir}/cmake/KF5GuiAddons/
%{_kf5_includedir}/KGuiAddons/
%{_kf5_archdatadir}/mkspecs/modules/qt_KGuiAddons.pri


%changelog
* Sat Nov 18 2023 Chair <chairou@tencent.com> - 5.110.0-1
- Init
